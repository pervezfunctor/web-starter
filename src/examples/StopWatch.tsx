import * as React from 'react'
import {
  Box,
  Button,
  CenteredText,
  Control,
  Field,
  Icon,
  Vertical,
} from '~/Components'
import { SFC } from '~/utils'

type StopWatchStatus = 'init' | 'paused' | 'started'

type StopWatchState = Readonly<{
  status: StopWatchStatus
  runningTime: number
}>

const initialState: StopWatchState = { status: 'init', runningTime: 0 }

type StopWatchViewProps = StopWatchState & {
  onStatusChange?(status: StopWatchStatus): void
  onReset?(): void
}

const StopWatchView: SFC<StopWatchViewProps> = ({
  status,
  runningTime,
  onStatusChange,
  onReset,
}) => (
  <Box>
    <CenteredText size="5">Stop Watch</CenteredText>
    <Vertical>
      <CenteredText size="2">{runningTime}</CenteredText>
      <Field grouped="grouped-centered">
        <Control>
          <Button
            onClick={() => {
              onStatusChange &&
                onStatusChange(status === 'started' ? 'paused' : 'started')
            }}
            variant="primary"
          >
            <Icon icon={status === 'started' ? 'pause' : 'play'} />
          </Button>
        </Control>
        <Control>
          <Button variant="danger" onClick={onReset}>
            <Icon icon="stop" />
          </Button>
        </Control>
      </Field>
    </Vertical>
  </Box>
)

export class StopWatch extends React.Component<{}, StopWatchState> {
  state: StopWatchState = initialState
  interval?: NodeJS.Timeout

  handleStatusChange = (status: StopWatchStatus) => {
    this.setState({ status })
    if (status === 'started') {
      this.interval = setInterval(() => {
        this.setState(state => ({
          runningTime: state.runningTime + 1,
        }))
      }, 1000)
    } else {
      if (this.interval) {
        clearInterval(this.interval)
      }
    }
  }

  handleReset = () => {
    this.setState(initialState)

    if (this.interval) {
      clearInterval(this.interval)
    }
  }

  componentWillUnmount() {
    if (this.interval) {
      clearInterval(this.interval)
    }
  }

  render(): JSX.Element {
    return (
      <StopWatchView
        {...this.state}
        onStatusChange={this.handleStatusChange}
        onReset={this.handleReset}
      />
    )
  }
}
