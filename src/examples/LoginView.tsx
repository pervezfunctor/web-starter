import * as React from 'react'

import {
  Button,
  Checkbox,
  Field,
  Label,
  Radio,
  RadioItem,
  TextBox,
  UnRadio,
  Control,
  Box,
} from '~/Components'
import { SFC } from '~/utils'

export const LoginView: SFC = () => (
  <Box>
    <Field>
      <Label>Username</Label>
      <Control>
        <TextBox placeholder="Enter User Name" />
      </Control>
    </Field>

    <Field>
      <Label>Password</Label>
      <Control>
        <TextBox type="password" placeholder="Enter Password" />
      </Control>
    </Field>

    <Field>
      <Control>
        <UnRadio defaultValue="male" name="gender">
          <RadioItem value="male"> Male</RadioItem>
          <RadioItem value="female"> Female</RadioItem>
        </UnRadio>
      </Control>
    </Field>

    <Field>
      <Control>
        <Checkbox> Remember Me</Checkbox>
      </Control>
    </Field>

    <Field grouped="grouped-right">
      <Control>
        <Button variant="primary">Submit</Button>
      </Control>
      <Control>
        <Button variant="danger">Reset</Button>
      </Control>
    </Field>
  </Box>
)
