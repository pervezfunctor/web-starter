import * as React from 'react'
import {
  Button,
  Checkbox,
  Control,
  Field,
  Label,
  Radio,
  Select,
  TextBox,
  UnRadio,
  TextArea,
  Help,
  Box,
} from '~/Components'

export const FieldsExample = () => (
  <Box>
    <Field addons="addons-right">
      <Control>
        <Select>
          <option>$</option>
          <option>£</option>
          <option>€</option>
        </Select>
      </Control>
      <Control>
        <TextBox placeholder="Amount of money" />
      </Control>
      <Control>
        <Button variant="primary">Transfer</Button>
      </Control>
    </Field>

    <Field>
      <Label>Username</Label>
      <Control>
        <TextBox
          variant="success"
          placeholder="Text input"
          readOnly
          value="bulma"
        />
      </Control>
      <Help variant="success">username is available</Help>
    </Field>

    <Field>
      <Label>Email</Label>
      <Control>
        <TextBox
          variant="danger"
          type="email"
          placeholder="Email input"
          value="hello@"
          readOnly
        />
      </Control>
      <Help variant="danger">This email is invalid</Help>
    </Field>

    <Field>
      <Label>Subject</Label>
      <Control>
        <Select>
          <option>React</option>
          <option>Angular</option>
          <option>Ember</option>
          <option>Reagent</option>
        </Select>
      </Control>
    </Field>

    <Field>
      <Label>Description</Label>
      <Control>
        <TextArea placeholder="Description" />
      </Control>
    </Field>

    <Field>
      <Control>
        <UnRadio name="question" defaultValue="yes" label="Are you Vegetarian?">
          <Radio.Item value="yes"> Yes</Radio.Item>
          <Radio.Item value="no"> No</Radio.Item>
        </UnRadio>
      </Control>
    </Field>

    <Field>
      <Control>
        <Checkbox>
          {' '}
          I agree to the <a href="#">terms and conditions</a>{' '}
        </Checkbox>
      </Control>
    </Field>

    <Field grouped="grouped-centered">
      <Button variant="primary">Submit</Button>
      <Button variant="danger">Reset</Button>
    </Field>
  </Box>
)
