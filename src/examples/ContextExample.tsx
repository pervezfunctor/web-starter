import * as React from 'react'
import { CenteredText, Box } from '~/Components'

const CounterContext = React.createContext({ count: 100 })

export const ContextExample = () => (
  <Box>
    <CounterContext.Provider value={{ count: 300 }}>
      <CounterContext.Provider value={{ count: 500 }}>
        <CounterContext.Consumer>
          {value => <CenteredText>{value.count}</CenteredText>}
        </CounterContext.Consumer>
      </CounterContext.Provider>
      <CounterContext.Consumer>
        {value => <CenteredText>{value.count}</CenteredText>}
      </CounterContext.Consumer>
    </CounterContext.Provider>

    <CounterContext.Provider value={{ count: 200 }}>
      <CounterContext.Consumer>
        {value => <CenteredText>{value.count}</CenteredText>}
      </CounterContext.Consumer>
    </CounterContext.Provider>

    <CounterContext.Provider value={{ count: 400 }}>
      <CounterContext.Consumer>
        {value => <CenteredText>{value.count}</CenteredText>}
      </CounterContext.Consumer>
    </CounterContext.Provider>

    <CounterContext.Consumer>
      {value => <CenteredText>{value.count}</CenteredText>}
    </CounterContext.Consumer>
  </Box>
)
