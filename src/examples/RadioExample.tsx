import * as React from 'react'
import { Box, CenteredText, Radio, UnRadio } from '~/Components'
import { SFC } from '~/utils'

const ControlledRadioExample = () => {
  const [value, set] = React.useState('green')

  return (
    <React.Fragment>
      <CenteredText size="4">{`Controlled Radio(${value})`}</CenteredText>
      <Radio
        name="color"
        selected={value}
        onSelectionChange={({ value }) => set(value)}
      >
        <Radio.Item value="red"> Red</Radio.Item>
        <Radio.Item value="green"> Green</Radio.Item>
        <Radio.Item value="blue"> Blue</Radio.Item>
      </Radio>
      <hr />
    </React.Fragment>
  )
}

const ReadonlyRadioExample = () => (
  <React.Fragment>
    <CenteredText size="4">Controlled Readonly Radio</CenteredText>
    <Radio name="color2" selected="green">
      <Radio.Item value="red"> Red</Radio.Item>
      <Radio.Item value="green"> Green</Radio.Item>
      <Radio.Item value="blue"> Blue</Radio.Item>
    </Radio>
    <hr />
  </React.Fragment>
)

const UncontrolledRadioExample = () => (
  <React.Fragment>
    <CenteredText size="4">Uncontrolled Radio</CenteredText>
    <UnRadio name="color3" defaultValue="red">
      <Radio.Item value="red"> Red</Radio.Item>
      <Radio.Item value="green"> Green</Radio.Item>
      <Radio.Item value="blue"> Blue</Radio.Item>
    </UnRadio>
  </React.Fragment>
)

export const RadioExample: SFC = () => (
  <Box>
    <ControlledRadioExample />
    <ReadonlyRadioExample />
    <UncontrolledRadioExample />
  </Box>
)
