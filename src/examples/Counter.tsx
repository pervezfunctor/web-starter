import * as React from 'react'
import { useState } from 'react'
import { Button, CenteredText, Horizontal, Icon, Vertical } from '~/Components'
import { SFC } from '~/utils'

type CounterProps = Readonly<{
  count: number
  onIncrement(): void
  onDecrement(): void
}>

const CounterView: SFC<CounterProps> = ({
  count,
  onIncrement,
  onDecrement,
}) => (
  <Horizontal>
    <Vertical>
      <Button variant="primary" onClick={onIncrement}>
        <Icon icon="plus" />
      </Button>

      <CenteredText size="3">{count}</CenteredText>

      <Button variant="danger" onClick={onDecrement}>
        <Icon icon="minus" />
      </Button>
    </Vertical>
  </Horizontal>
)

export const Counter = () => {
  const [count, set] = useState(0)

  const inc = () => set(count + 1)
  const dec = () => set(count - 1)

  return <CounterView count={count} onIncrement={inc} onDecrement={dec} />
}
