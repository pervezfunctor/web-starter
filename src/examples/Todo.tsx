import { useImmerReducer } from 'use-immer'
import * as React from 'react'
import { ulid } from 'ulid'
import {
  Box,
  Button,
  CenteredText,
  Checkbox,
  Table,
  TableBody,
  TableDataRow,
  TableHeader,
  TextBox,
} from '~/Components'
import { classNames, SFC } from '~/utils'

interface Todo {
  id: string
  title: string
  done: boolean
}

type TodoList = Todo[]

const createTodo = (title: string, done = false) => ({
  id: ulid(),
  title,
  done,
})

type AddTodoProps = { onAddTodo(title: string): void }

const AddTodoView: SFC<AddTodoProps> = ({ onAddTodo }) => (
  <TextBox
    variant="primary"
    placeholder="Add Todo"
    onKeyUp={evt => {
      if (evt.keyCode === 13) {
        onAddTodo(evt.currentTarget.value)
        evt.currentTarget.value = ''
      }
    }}
  />
)

interface TodoItemProps {
  readonly todo: Todo
  onToggle(id: string): void
  onRemove(id: string): void
}

class TodoItem extends React.Component<TodoItemProps> {
  render() {
    const {
      todo: { id, title, done },
      onToggle,
      onRemove,
    } = this.props
    return (
      <TableDataRow className={classNames({ 'has-text-grey-lighter': done })}>
        <td>{id}</td>
        <td>{title}</td>
        <td>
          <Checkbox checked={done} onChange={() => onToggle(id)} />
        </td>
        <td>
          <Button variant="danger" onClick={() => onRemove(id)}>
            Delete
          </Button>
        </td>
      </TableDataRow>
    )
  }
}

interface TodoListViewProps {
  readonly todoList: TodoList
  onToggle(id: string): void
  onRemove(id: string): void
}

const TodoListView: SFC<TodoListViewProps> = ({
  todoList,
  onToggle,
  onRemove,
}): JSX.Element => (
  <div>
    {todoList.length === 0 ? (
      <CenteredText className="title">Todo List is empty!</CenteredText>
    ) : (
      <Table>
        <TableHeader headers={['Id', 'Title', 'Completed', '']} />
        <TableBody>
          {todoList.map<React.ReactElement<typeof TableDataRow>>(t => (
            <TodoItem
              todo={t}
              key={t.id}
              onToggle={onToggle}
              onRemove={onRemove}
            />
          ))}
        </TableBody>
      </Table>
    )}
  </div>
)

type TodoAction =
  | { type: 'TOGGLE'; id: string }
  | { type: 'REMOVE'; id: string }
  | { type: 'ADD'; title: string }

const todoReducer = (draft: TodoList, action: TodoAction) => {
  switch (action.type) {
    case 'ADD':
      draft.push(createTodo(action.title))
      break
    case 'REMOVE':
      const i = draft.findIndex(t => t.id === action.id)
      if (i !== -1) draft.splice(i, 1)
      break
    case 'TOGGLE':
      const todo = draft.find(t => t.id === action.id)
      if (todo) todo.done = !todo.done
      break
  }
}

export const Todo = () => {
  const [todoList, dispatch] = useImmerReducer<TodoList, TodoAction>(
    todoReducer,
    [],
  )
  return (
    <Box>
      <AddTodoView onAddTodo={title => dispatch({ type: 'ADD', title })} />
      <TodoListView
        todoList={todoList}
        onToggle={id => dispatch({ type: 'TOGGLE', id })}
        onRemove={id => dispatch({ type: 'REMOVE', id })}
      />
    </Box>
  )
}
