import * as React from 'react'
import { Omit } from 'react-router'
import { classNames, h, SFC, capitalize } from '~/utils'
import { FlexDirectionProperty, FlexWrapProperty } from 'csstype'

type Size = 'small' | 'medium' | 'large'
type Variant = 'primary' | 'danger' | 'info' | 'success'

type Props<T = {}, P = React.HTMLAttributes<HTMLElement>> = P & Readonly<T>

export const Box = h<Props>('box')

export const Label = h<Props>({ tag: 'label', classes: 'label' })

export type HelpProps = Props<{ variant?: Variant }>

export const Help = h<HelpProps>({
  tag: 'p',
  classes: ({ variant }) => ['help', { [`is-${variant}`]: variant }],
})

export type ControlProps = Props<{
  loading?: boolean
  expanded?: boolean
  size?: Size
}>

export const Control = h<ControlProps>(({ loading, expanded, size }) => [
  'control',
  {
    'is-loading': loading,
    'is-expanded': expanded,
    [`is-${size}`]: size,
  },
])

export type FieldProps = Props<{
  horizontal?: boolean
  grouped?: 'grouped' | 'grouped-centered' | 'grouped-right'
  addons?: 'addons' | 'addons-centered' | 'addons-right'
}>

export const Field = h<FieldProps>(({ horizontal, grouped, addons }) => [
  'field',
  {
    'is-horizontal': horizontal,
    'is-grouped': grouped,
    [`is-${grouped}`]:
      grouped === 'grouped-centered' || grouped === 'grouped-right',
    'has-addons': addons,
    [`has-${addons}`]:
      addons === 'addons-centered' || addons === 'addons-right',
  },
])

type ButtonProps = Props<
  {
    variant?: Variant
    size?: Size
  },
  React.ButtonHTMLAttributes<HTMLButtonElement>
>

export const Button = h<ButtonProps>({
  tag: 'button',
  classes: ({ variant, size }) => [
    'button',
    {
      [`is-${variant}`]: variant,
      [`is-${size}`]: size,
    },
  ],
})

type TextProps = Props<{ size?: '1' | '2' | '3' | '4' | '5' | '6' | '7' }>

export const CenteredText = h<TextProps>(({ size }) => [
  'has-text-centered',
  { [`is-size-${size}`]: size },
])

type InputProps<T> = Props<T, React.InputHTMLAttributes<HTMLInputElement>>

type CheckboxProps = InputProps<{ size?: Size }>

export const Checkbox: React.SFC<CheckboxProps> = ({
  size,
  children,
  className,
  ...props
}) => (
  <label className="checkbox">
    <input
      {...props}
      type="checkbox"
      className={classNames('checkbox', { [`is-${size}`]: size }, className)}
    />
    {children}
  </label>
)

type TextBoxProps = InputProps<{ variant?: Variant; size?: Size }>

export const TextBox = h<TextBoxProps>({
  tag: 'input',
  classes: ({ size, variant }) => [
    'input',
    {
      [`is-${size}`]: size,
      [`is-${variant}`]: variant,
    },
  ],
})

type TextAreaProps = InputProps<{ variant?: Variant; size?: Size }>

export const TextArea = h<TextAreaProps>({
  tag: 'textarea',
  classes: ({ size, variant }) => [
    'textarea',
    {
      [`is-${size}`]: size,
      [`is-${variant}`]: variant,
    },
  ],
})

export type SelectionEvent<Value = string> = Readonly<{
  name: string
  value: Value
}>

type SelectProps = Props<
  {
    color?: Variant
    state?: 'hovered' | 'focused'
    size?: Size
  },
  React.SelectHTMLAttributes<HTMLSelectElement>
>

export const Select: SFC<SelectProps> = ({
  size,
  color,
  state,
  children,
  className,
  ...props
}) => (
  <div className={classNames('select', { [`is-${color}`]: color })}>
    <select
      {...props}
      className={classNames(
        {
          [`is-${state}`]: state,
        },
        className,
      )}
    >
      {children}
    </select>
  </div>
)

export type RadioItemProps = Omit<
  InputProps<{ value: string }>,
  'name' | 'onChange' | 'checked'
>

export const RadioItem: SFC<RadioItemProps> = ({ children, ...props }) => (
  <label className="radio">
    <input type="radio" {...props} />
    {children}
  </label>
)

export type RadioProps = Props<{
  name: string
  selected: string
  label?: string
  onSelectionChange?(evt: SelectionEvent): void
}>

export const Radio: SFC<RadioProps> & { Item: typeof RadioItem } = ({
  name,
  selected,
  onSelectionChange,
  label = capitalize(name),
  children,
  ...props
}) => (
  <div {...props}>
    <Label>{label}</Label>
    {React.Children.map(children, (child: any) =>
      React.cloneElement(child, {
        name,
        readOnly: !!onSelectionChange,
        checked: child.props.value === selected,

        onChange: (evt: React.ChangeEvent<HTMLInputElement>) =>
          onSelectionChange &&
          onSelectionChange({ name, value: evt.currentTarget.value }),
      }),
    )}
  </div>
)

Radio.Item = RadioItem

type UnRadioProps = Props<
  { defaultValue: string },
  Omit<RadioProps, 'selected' | 'onSelectionChange'>
>

export const UnRadio: React.SFC<UnRadioProps> & { Item: typeof RadioItem } = ({
  defaultValue,
  ...props
}) => {
  const [selected, setSelected] = React.useState(defaultValue)
  return (
    <Radio
      {...props}
      selected={selected}
      onSelectionChange={({ value }) => setSelected(value)}
    />
  )
}

UnRadio.Item = RadioItem

type IconProps = Props<{ icon: 'minus' | 'plus' | 'pause' | 'play' | 'stop' }>

export const Icon: SFC<IconProps> = ({ icon, className, ...props }) => (
  <span {...props} className={classNames('icon', className)}>
    <i className={`fas fa-${icon}`} />
  </span>
)

type FlexProps = Props<{
  direction?: FlexDirectionProperty
  wrap?: FlexWrapProperty
}>

export const Flex: SFC<FlexProps> = ({ direction, wrap, children }) => (
  <div
    style={{
      display: 'flex',
      flexDirection: direction || 'row',
      flexWrap: wrap || 'wrap',
    }}
  >
    {children}
  </div>
)

export const Horizontal: SFC<Omit<FlexProps, 'direction'>> = props => (
  <Flex direction="row" {...props} />
)

export const Vertical: SFC<Omit<FlexProps, 'direction'>> = props => (
  <Flex direction="column" {...props} />
)

export type SliderProps = InputProps<{ variant?: Variant; size?: Size }>

export const Slider = h<SliderProps>({
  tag: 'input',
  props: { type: 'range' },

  classes: ({ variant }) => [
    'slider',
    'is-fullwidth',
    { [`is-${variant}`]: variant },
  ],
})

type RowProps = React.HTMLAttributes<HTMLTableRowElement>

type TableHeaders = Props<{ headers: ReadonlyArray<string> }, RowProps>

export const TableHeader: SFC<TableHeaders> = ({ headers }) => (
  <thead>
    <tr>
      {headers.map(h => (
        <th key={h}>{h}</th>
      ))}
    </tr>
  </thead>
)

export const TableBody: React.SFC = props => <tbody {...props} />

export const TableDataRow = h<RowProps>({ tag: 'tr' })

export type TableProps = Props<
  {
    striped?: boolean
    bordered?: boolean
    fullWidth?: boolean
  },
  React.TableHTMLAttributes<HTMLTableElement>
>

export const Table = h<TableProps>({
  tag: 'table',
  classes: ({ striped, bordered, fullWidth }) => [
    'table',
    {
      'is-striped': striped,
      'is-bordered': bordered,
      'is-fullwidth': fullWidth,
    },
  ],
})

export type RGBValue = Readonly<{
  red: number
  green: number
  blue: number
}>

export type RGBChangeEvent = Readonly<{
  name: string
  value?: RGBValue
}>

type RGBViewProps = Readonly<{
  name: string
  value: RGBValue
  onChange?(event: RGBChangeEvent): void
}>

const RGBView: SFC<RGBViewProps> = ({ name, onChange, value }) => {
  const defaultProps = { min: 0, max: 255 }

  const update = (args: Partial<RGBValue>) =>
    onChange && onChange({ name, value: { ...value, ...args } })

  return (
    <div className="rgb-view box">
      <CenteredText>RGB</CenteredText>

      <Slider
        {...defaultProps}
        name="red"
        variant="danger"
        value={value.red}
        onChange={evt => update({ red: +evt.currentTarget.value })}
      />

      <Slider
        {...defaultProps}
        name="green"
        variant="primary"
        value={value.green}
        onChange={evt => update({ green: +evt.currentTarget.value })}
      />

      <Slider
        {...defaultProps}
        name="blue"
        variant="info"
        value={value.blue}
        onChange={evt => update({ blue: +evt.currentTarget.value })}
      />
    </div>
  )
}

export type RGBProps = RGBViewProps

export const RGB: SFC<RGBProps> = ({ name, value, onChange }) => (
  <RGBView
    name={name}
    value={value}
    onChange={evt => onChange && onChange(evt)}
  />
)
