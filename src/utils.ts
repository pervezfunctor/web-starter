import * as React from 'react'
import * as t from 'tcomb'

export type SFC<P = {}> = React.SFC<P>

export const capitalize = (str: string) => str[0].toUpperCase() + str.slice(1)

export const capitalizeAll = (str: string) =>
  str
    .split(' ')
    .map(capitalize)
    .join(' ')

export type ClassNamesArg =
  | null
  | undefined
  | string
  | string[]
  | { [index: string]: unknown }

export const classNames = (...args: ClassNamesArg[]): string => {
  const draft: string[] = []

  for (const arg of args) {
    if (t.String.is(arg)) {
      if (arg !== '') draft.push(arg)
    } else if (t.Array.is(arg)) {
      const res = classNames(...(arg as any))
      if (res !== '') draft.push(res)
    } else if (t.Object.is(arg)) {
      for (const key of Object.keys(arg)) {
        if (arg[key]) draft.push(key)
      }
    }
    // tslint:disable-next-line strict-type-predicates
    else if (arg !== null && arg !== undefined) {
      throw new Error(`classNames cannot handle ${arg}`)
    }
  }

  return draft.join(' ')
}

export const toLower = (arg: string, delimiter: string = ' ') => {
  if (arg.length === 0) return ''
  let result: string = arg[0].toLowerCase()
  for (const v of arg.slice(1)) {
    result += v === v.toUpperCase() ? `${delimiter}${v.toLowerCase()}` : v
  }
  return result
}

type HClass<Props> =
  | string
  | ClassNamesArg[]
  | ((props: Props) => string | ClassNamesArg[])

type HObject<Props> = {
  tag?: string
  classes?: HClass<Props>
  props?: Props
}
type HArgs<Props> = HClass<Props> | HObject<Props>

const H: <Props>(args: HObject<Props>) => SFC<Props> = ({
  tag = 'div',
  classes,
  props,
}) => innerProps =>
  React.createElement(tag, {
    ...props,
    ...innerProps,
    className: classNames(
      t.Function.is(classes) ? classes(innerProps) : classes,
    ),
  })

export const h: <Props>(args: HArgs<Props>) => SFC<Props> = args =>
  t.Function.is(args) || t.Array.is(args) || t.String.is(args)
    ? H({ classes: args })
    : H(args)
